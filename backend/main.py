from typing import Union
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Node:
    __objects = []

    def all():
        return Node.__objects

    def find(node_id):
        return Node.__objects[node_id]

    def save(self):
        node_id = len(Node.__objects) + 1 
        Node.__objects.append(self)
        self.id = node_id


@app.get("/")
def read_root():
    return {"Hello": "World"}




class Item(BaseModel, Node):
    id: int
    name: str
    price: float
    is_offer: Union[bool, None] = None

@app.get("/items")
def index_item():
    return Item.all()

@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    item = Item.find(item_id)
    return item


@app.put("/items")
def update_item(item: Item):
    item.save()
    return item




class Counter(BaseModel, Node):
    value: int

g_counter = Counter(value=29)

@app.get("/counter")
def index_item():
    global g_counter
    return g_counter.value

@app.put("/counter")
def update_item(item: Item):
    global g_counter
    g_counter.value = item.value
    return g_counter.value