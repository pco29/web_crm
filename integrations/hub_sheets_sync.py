import json
import os
import re
from datetime import datetime
from typing import Optional

import gspread  # gspread
import pandas as pd  # pandas 
import requests  # requests
from google.oauth2 import service_account  # google-auth-oauthlib


def pull_sheets(sheets: dict[str, list[str]]) -> Optional[pd.DataFrame]:
    credentials = service_account.Credentials.from_service_account_file(
        "service.json"
    ).with_scopes(["https://spreadsheets.google.com/feeds"])
    client = gspread.authorize(credentials)
    df = None
    for url, worksheets in sheets.items():
        for worksheet in worksheets:
            values = client.open_by_url(url).worksheet(worksheet).get_all_values()
            if not df:
                df = pd.DataFrame.from_records(values[1:], columns=values[0])
            else:
                pd.concat(df, pd.DataFrame.from_records(values[1:], columns=values[0]))
    return df


def _normalize_name(name: str) -> list[str]:
    return " ".join([n.strip().lower().capitalize() for n in name.strip().split()])


def _normalize_tel(tel: str) -> list[str]:
    tel = re.sub(r"(\D*)", "", tel)
    if len(tel) > 11 and tel.startswith("55"):
        return f"+{tel}"
    if len(tel) in (10, 11):
        return f"+55{tel}"
    return tel


def prepare_csv(df: pd.DataFrame) -> str:
    df = df[df["nome"] != ""].copy()  # drop entries without name
    df["nome"] = df["nome"].map(_normalize_name)
    df["UF"] = df["UF"].map(str.upper)
    df["cidade"] = df["cidade"].map(_normalize_name)
    df["grupo"] = df["grupo"].map(str.upper)
    df["responsável"] = df["responsável"].map(_normalize_name)
    df["número"] = df["número"].map(_normalize_tel)
    df["firstname"] = df["nome"].map(lambda n: n.split()[0])
    df["lastname"] = df["nome"].map(
        lambda n: " ".join(n.split()[1:]) if len(n.split()) > 1 else ""
    )

    cdf = df[(df["UF"] != "ZZ") & (df["UF"] != "")]  # drop ZZ and no UF
    cdf = cdf[cdf.groupby("nome")["nome"].transform("size") == 1]  # remove duplicates
    cdf = cdf[
        (cdf["grupo"] != "G4F") & (cdf["grupo"] != "SG") & (cdf["grupo"] != "")
    ]  # cleanup grupo

    export_name = f"{datetime.utcnow().isoformat()}.csv"
    cdf[["firstname", "lastname", "UF", "cidade", "número", "grupo"]].sort_values(
        "firstname"
    ).to_csv(export_name, index=False)

    return export_name


def push_hub(export: str):
    url = "https://api.hubapi.com/crm/v3/imports"
    token = os.environ["HUBSPOT_TOKEN"]
    headers = {"authorization": f"Bearer {token}"}

    data = {
        "name": "47 Universidade de Férias",
        "dateFormat": "DAY_MONTH_YEAR",
        "files": [
            {
                "fileName": export,
                "fileFormat": "CSV",
                "fileImportPage": {
                    "hasHeader": True,
                    "columnMappings": [
                        {
                            "columnObjectTypeId": "0-1",
                            "columnName": "firstname",
                            "propertyName": "firstname",
                        },
                        {
                            "columnObjectTypeId": "0-1",
                            "columnName": "lastname",
                            "propertyName": "lastname",
                        },
                        {
                            "columnObjectTypeId": "0-1",
                            "columnName": "uf",
                            "propertyName": "uf",
                        },
                        {
                            "columnObjectTypeId": "0-1",
                            "columnName": "cidade",
                            "propertyName": "city",
                        },
                        {
                            "columnObjectTypeId": "0-1",
                            "columnName": "numero",
                            "propertyName": "phone",
                        },
                        {
                            "columnObjectTypeId": "0-1",
                            "columnName": "grupo",
                            "propertyName": "grupo",
                        },
                    ],
                },
            }
        ],
    }
    datastring = json.dumps(data)
    payload = {"importRequest": datastring}
    files = [("files", open(export, "rb"))]
    requests.request("POST", url, data=payload, files=files, headers=headers)


if __name__ == "__main__":
    df = pull_sheets(
        {
            "https://docs.google.com/spreadsheets/d/1nvyXqxtC0AljFmeKCOAJ1gDr-hzDW_nUFLr3fEcKgFc/edit?pli=1#gid=1051056677": [
                "convocação"
            ]
        }
    )
    export = prepare_csv(df)
    push_hub(export)
