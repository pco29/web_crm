# Preparar ambiente
```
python3 -m venv env
. ./env/bin/activate
pip install gspread pandas requests
```

Além do ambiente python são necessárias duas informações externas:

1. A variável de ambiente `HUBSPOT_TOKEN` que refere-se ao token necessário para utilizar a API do Hubspot (pode ser obtido no portal para desenvolvedores da ferramenta)
2. O arquivo `service.json`, com as credenciais necessárias para acessar arquivos no Google Drive
