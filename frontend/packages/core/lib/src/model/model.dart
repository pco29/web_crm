import 'package:equatable/equatable.dart';

abstract class Model extends Equatable {
  const Model();

  const Model.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson();

  @override
  List<Object?> get props;
}
