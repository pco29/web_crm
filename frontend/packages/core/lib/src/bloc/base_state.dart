part of 'base_bloc.dart';

abstract class BaseState extends Equatable {
  const BaseState();

  const BaseState.fromLastState(BaseState lastState);
}
