import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

abstract class Store<T> {
  final String key;
  SharedPreferences? _sharedPreferences;

  Store({required this.key});

  Future<T?> load() async {
    await _init();
    final loaded = _sharedPreferences!.get(key) as String?;
    if (loaded == null) {
      return null;
    }
    return fromJson(jsonDecode(loaded) as Map<String, dynamic>);
  }

  Future<void> set(dynamic value) async {
    await _init();
    await _sharedPreferences!.setString(key, jsonEncode(toJson(value)));
  }

  Future<void> _init() async {
    _sharedPreferences ??= await SharedPreferences.getInstance();
  }

  T fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson(T object);
}
