import 'dart:convert';

import 'package:http/http.dart' as http;

abstract class Client<T> {
  final String uri;
  Client({required this.uri});

  final String _urlBase = 'http://localhost:8000/';
  http.Client? _httpClient;
  http.Client get instance => _httpClient ?? http.Client();

  Future<T> get() async {
    http.Response response = await instance.get(Uri.parse('$_urlBase$uri'));
    return fromJson(jsonDecode(response.body) as Map<String, dynamic>);
  }

  T fromJson(Map<String, dynamic> json);
}
