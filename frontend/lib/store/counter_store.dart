import 'package:crm_app/model/counter.dart';
import 'package:core/features/store.dart';

class CounterStore extends Store<Counter> {
  CounterStore() : super(key: 'counter');

  @override
  Counter fromJson(Map<String, dynamic> json) {
    return Counter.fromJson(json);
  }

  @override
  Map<String, dynamic> toJson(Counter object) {
    return object.toJson();
  }
}
