import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:crm_app/bloc/counter_bloc.dart';
import 'package:crm_app/client/counter_client.dart';
import 'package:crm_app/repository/counter_repository.dart';
import 'package:crm_app/store/counter_store.dart';

class CounterProvider extends StatelessWidget {
  final Widget child;
  const CounterProvider({required this.child, super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CounterBloc>(
      create: (context) => CounterBloc(
        CounterRepository(
          CounterClient(),
          CounterStore(),
        ),
      )..add(CounterLoad()),
      child: child,
    );
  }
}
