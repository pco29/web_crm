import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:crm_app/bloc/counter_bloc.dart';

class CounterScreen extends StatelessWidget {
  const CounterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Counter'),
      ),
      body: const _CounterBody(),
      floatingActionButton: const _CounterButtons(),
    );
  }
}

class _CounterBody extends StatelessWidget {
  const _CounterBody();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const <Widget>[
          _CounterHeader(),
          _CounterBlocBuilder(),
        ],
      ),
    );
  }
}

class _CounterHeader extends StatelessWidget {
  const _CounterHeader();

  @override
  Widget build(BuildContext context) {
    return const Text(
      'You have pushed the button this many times:',
    );
  }
}

class _CounterBlocBuilder extends StatelessWidget {
  const _CounterBlocBuilder();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CounterBloc, CounterState>(
      builder: (context, state) => Column(
        children: [
          Text(
            '${state.counter}',
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          _CounterError(state),
        ],
      ),
    );
  }
}

class _CounterError extends StatelessWidget {
  final CounterState state;
  const _CounterError(this.state);

  @override
  Widget build(BuildContext context) {
    if (state is! CounterFail) {
      return const SizedBox();
    }
    return Text(
      (state as CounterFail).errorMsg,
      style: Theme.of(context).textTheme.headlineSmall?.copyWith(
            color: Theme.of(context).colorScheme.error,
          ),
    );
  }
}

class _CounterButtons extends StatelessWidget {
  const _CounterButtons();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: const [
        _CounterAddButton(),
        SizedBox(height: 16),
        _CounterSubtractButton(),
      ],
    );
  }
}

class _CounterAddButton extends StatelessWidget {
  const _CounterAddButton();

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () => _incrementCounter(context),
      tooltip: 'Increment',
      child: const Icon(Icons.add),
    );
  }

  void _incrementCounter(BuildContext context) {
    context.read<CounterBloc>().add(CounterAdd(1));
  }
}

class _CounterSubtractButton extends StatelessWidget {
  const _CounterSubtractButton();

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () => _decrementCounter(context),
      tooltip: 'Dencrement',
      child: const Icon(Icons.remove),
    );
  }

  void _decrementCounter(BuildContext context) {
    context.read<CounterBloc>().add(CounterAdd(-1));
  }
}
