import 'package:crm_app/model/counter.dart';
import 'package:core/features/client.dart';

class CounterClient extends Client<Counter> {
  CounterClient() : super(uri: 'counter');

  @override
  Counter fromJson(Map<String, dynamic> json) => Counter.fromJson(json);
}
