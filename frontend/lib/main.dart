import 'package:crm_app/provider/counter_provider.dart';
import 'package:flutter/material.dart';
import 'package:crm_app/screen/counter_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
        useMaterial3: true,
      ),
      home: const CounterProvider(child: CounterScreen()),
    );
  }
}
