import 'package:core/features/model.dart';

class Counter extends Model {
  final int count;

  const Counter(this.count);

  @override
  Counter.fromJson(Map<String, dynamic> json) : count = json['count'] as int;

  @override
  Map<String, dynamic> toJson() => {'count': count};

  @override
  List<Object?> get props => [count];
}
