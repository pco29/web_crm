import 'package:crm_app/client/counter_client.dart';
import 'package:crm_app/model/counter.dart';
import 'package:crm_app/store/counter_store.dart';

class CounterRepository {
  final CounterClient counterClient;
  final CounterStore counterStore;

  CounterRepository(this.counterClient, this.counterStore);

  Future<int?> load() async {
    final counter = await counterStore.load();
    return counter?.count;
  }

  Future<Counter?> get() async {
    return await counterClient.get();
  }

  Future<void> save(int valueToSave) async {
    final counter = Counter(valueToSave);
    await counterStore.set(counter);
  }

  Future<Counter> add(int valueToAdd) async {
    int count = await load() ?? 0;
    count += valueToAdd;
    final counter = Counter(count);
    await counterStore.set(counter);
    return counter;
  }

  post(int valueToAdd) {}
}
