class CounterState {
  final int counter;

  const CounterState(this.counter);
}

class CounterSuccess extends CounterState {
  CounterSuccess(super.counter);
}

class CounterFail extends CounterState {
  final String errorMsg;
  CounterFail(super.counter, this.errorMsg);
}
