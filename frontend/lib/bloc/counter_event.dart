abstract class CounterEvent {}

class CounterLoad extends CounterEvent {}

class CounterAdd extends CounterEvent {
  final int valueToAdd;

  CounterAdd(this.valueToAdd);
}
