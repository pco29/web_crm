import 'dart:async';

import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:crm_app/model/counter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:crm_app/repository/counter_repository.dart';

import 'counter_event.dart';
import 'counter_state.dart';

export 'counter_event.dart';
export 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  final CounterRepository counterRepository;

  CounterBloc(
    this.counterRepository,
  ) : super(const CounterState(0)) {
    on<CounterLoad>(_onCounterLoad);
    on<CounterAdd>(_onCounterAdd, transformer: restartable());
  }

  FutureOr<void> _onCounterLoad(
    CounterLoad event,
    Emitter<CounterState> emit,
  ) async {
    try {
      final counterLocal = await counterRepository.load() ?? 0;
      Counter? counterSincronizado;
      try {
        counterSincronizado = await counterRepository.get();
      } catch (e) {
        print(e);
      }
      final int counter;

      if (counterLocal <= (counterSincronizado?.count ?? 0)) {
        await counterRepository.save(counterSincronizado?.count ?? 0);
        counter = counterSincronizado?.count ?? 0;
      } else {
        await counterRepository.save(counterLocal);
        counter = counterLocal;
      }

      emit(CounterSuccess(counter));
    } catch (error) {
      emit(CounterFail(state.counter, error.toString()));
    }
  }

  FutureOr<void> _onCounterAdd(
    CounterAdd event,
    Emitter<CounterState> emit,
  ) async {
    try {
      final counterLocal = await counterRepository.add(event.valueToAdd);
      int counterSincronizado = 0;
      try {
        counterSincronizado = await counterRepository.post(event.valueToAdd);
      } catch (e) {
        print(e);
      }
      final int counter;

      if (counterLocal.count <= counterSincronizado) {
        await counterRepository.save(counterSincronizado);
        counter = counterSincronizado;
      } else {
        await counterRepository.save(counterLocal.count);
        counter = counterLocal.count;
      }

      emit(CounterSuccess(counter));
    } catch (error) {
      emit(CounterFail(state.counter, error.toString()));
    }
  }
}
